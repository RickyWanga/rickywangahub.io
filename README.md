# rickywanga.io
# https://rickywanga.github.io/
Sito v.2.0

# Progetto Alternanza
Questa repository contiene il codice del sito web (statico e senza javascript poiché richiesto dai prof) riguardante il terzo anno di Alternanza Scuola-Lavoro e i vari eventi ai quali abbiamo partecipato.
La galleria immaggini è stata ommessa per motivi di privacy, al momento sto provvedendo ad ottentere il consenso di poter pubblicare anche la galleria.

# Alcune guide-lines
Data la natura statica del sito, il sito è ottimizzato per i schermi con una risoluzione di 1920x1080 pixel.
Per poter visulizzare il sito potete accedere al link della repository sopra linkato, ma si consiglia fortemente di clonare la repository oppure di esportarla in formato zip.
